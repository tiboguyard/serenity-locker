package com.example.serenitylocker.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.serenitylocker.Middlewares.ApiRequestHandler;
import com.example.serenitylocker.Model.User;
import com.example.serenitylocker.Model.UserC;
import com.example.serenitylocker.R;
import com.example.serenitylocker.View.MainActivity;

import java.util.Objects;

public class SignUpFragment extends Fragment {
    boolean isPasswordVisible = false;
    boolean isConfirmPasswordVisible = false;

    public SignUpFragment() {}
    @NonNull
    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View signUpFragmentView = inflater.inflate(R.layout.fragment_sign_up, container, false);

        EditText firstNameEditText = signUpFragmentView.findViewById(R.id.firstName);
        EditText lastNameEditText = signUpFragmentView.findViewById(R.id.lastName);
        EditText mailEditText = signUpFragmentView.findViewById(R.id.mailAddress);
        EditText passwordEditText = signUpFragmentView.findViewById(R.id.password);
        EditText confirmPasswordEditText = signUpFragmentView.findViewById(R.id.password_confirm);

        /**
         * Evénement qui permet de montrer ou cacher le mot de passe
         */
        Button showHidePasswordButton = signUpFragmentView.findViewById(R.id.showHidePassword);
        showHidePasswordButton.setOnClickListener(view -> {
            isPasswordVisible = !isPasswordVisible;
            if (isPasswordVisible) {
                passwordEditText.setTransformationMethod(null);
                showHidePasswordButton.setText(R.string.hide_label);
            } else {
                passwordEditText.setTransformationMethod(new PasswordTransformationMethod());
                showHidePasswordButton.setText(R.string.show_label);
            }
        });

        /**
         * Evénement qui permet de montrer ou cacher le mot de passe de confirmation
         */
        Button showHideConfirmPasswordButton = signUpFragmentView.findViewById(R.id.showHidePassword_confirm);
        showHideConfirmPasswordButton.setOnClickListener(view -> {
            isConfirmPasswordVisible = !isConfirmPasswordVisible;
            if (isConfirmPasswordVisible) {
                confirmPasswordEditText.setTransformationMethod(null);
                showHideConfirmPasswordButton.setText(R.string.hide_label);
            } else {
                confirmPasswordEditText.setTransformationMethod(new PasswordTransformationMethod());
                showHideConfirmPasswordButton.setText(R.string.show_label);
            }
        });

        /**
         * Evénement qui permet de gérer la pression sur le bouton "Sign up"
         */
        Button signUpButton = signUpFragmentView.findViewById(R.id.logInButton);
        signUpButton.setOnClickListener(view -> {
            boolean isFormValid = !firstNameEditText.getText().toString().trim().equals("") &&
                    !lastNameEditText.getText().toString().trim().equals("") &&
                    !mailEditText.getText().toString().trim().equals("") &&
                    !passwordEditText.getText().toString().trim().equals("") &&
                    !confirmPasswordEditText.getText().toString().trim().equals("") &&
                    passwordEditText.getText().toString().trim().equals(confirmPasswordEditText.getText().toString().trim());

            if (isFormValid) {
                ApiRequestHandler apiHandler = new ApiRequestHandler(
                        new String[]{"Users"},
                        new UserC(lastNameEditText.getText().toString(), firstNameEditText.getText().toString(), passwordEditText.getText().toString(), mailEditText.getText().toString())
                );
                apiHandler.sendARequest(ApiRequestHandler.Requests.CreateAnAccount);
                try {
                    if (apiHandler.isApiResponseValid()) {
                        User apiResponse = apiHandler.getApiResponse(User.class);
                        // Sauvegarde de l'id de la personne dans le context de l'application
                        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("LoggedUserInformations", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("loggedUserId", apiResponse.getId());
                        editor.apply();

                        // Lancement de l'activitée principal
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.wrong_credentials), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.wrong_form), Toast.LENGTH_SHORT).show();
            }
        });

        /**
         * Evénement qui permet de changer le "fragment" pour celui de la connection à un compte
         */
        Button signInRequest = signUpFragmentView.findViewById(R.id.signIn);
        signInRequest.setOnClickListener(view -> {
            Fragment logInFragment = new LogInFragment();
            getParentFragmentManager().beginTransaction().replace(R.id.SignInUpFrame, logInFragment).commit();
        });


        return signUpFragmentView;
    }
}