package com.example.serenitylocker.Model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Cette classe représente les informations d'un Locker
 */
public class LockerInfos {
    /**
     * Numéro de série unique à chaque Locker
     */
    @Nullable
    @SerializedName("SerialNumber")
    private String serialNumber;
    /**
     * Etat actuel du Locker
     * true : le locker est en position fermé
     * false : le locker est en position ouvert
     */
    @SerializedName("IsLocked")
    private Boolean isLocked;
    /**
     * Date et heure de la dernière modification d'état
     */
    @Nullable
    @SerializedName("LastInteraction")
    private String lastInteraction;
    /**
     * Historique de toutes les modifications d'états
     */
    @Nullable
    @SerializedName("History")
    private List<HistoryLine> history;


    @Nullable
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(@Nullable String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Boolean getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(Boolean isLocked) {
        this.isLocked = isLocked;
    }

    @Nullable
    public String getLastInteraction() {
        return lastInteraction;
    }

    public void setLastInteraction(@Nullable String lastInteraction) {
        this.lastInteraction = lastInteraction;
    }

    @Nullable
    public List<HistoryLine> getHistory() {
        return history;
    }

    public void setHistory(@Nullable List<HistoryLine> history) {
        this.history = history;
    }
}
