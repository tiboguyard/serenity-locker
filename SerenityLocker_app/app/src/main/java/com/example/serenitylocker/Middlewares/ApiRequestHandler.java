package com.example.serenitylocker.Middlewares;

import android.annotation.SuppressLint;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.Objects;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Classe qui permet de gérer les requêtes sur l'API
 */
public class ApiRequestHandler {
    private final String API_IP = "192.168.1.78:45455";
    private final String[] apiPaths;
    private final String[] apiUriParametersName;
    private final String[] apiUriParametersValue;
    private final Object requestBody;
    private String apiResponse;
    private int apiResponseCode;
    private final Gson gson;
    private final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    /**
     * Enumération de toutes les requêtes possibles
     */
    public enum Requests {
        Authenticate,
        CreateAnAccount,
        GetUser,
        GetUserHistory,
        BindLockerToUser,
        UnbindLockerOfUser,
        UpdateUserHistory,
        DeleteUserHistory
    }

    /**
     * Constructeur
     * @param paths - Chemins (endpoints) de l'api
     */
    public ApiRequestHandler(String[] paths) {
        this(paths, new String[] {}, new String[] {}, "");
    }

    /**
     * Constructeur
     * @param paths - Chemins (endpoints) de l'api
     * @param body - Corps de la requête
     */
    public <T> ApiRequestHandler(String[] paths, T body) {
        this(paths, new String[] {}, new String[] {}, body);
    }

    /**
     * Constructeur
     * @param paths - Chemins (endpoints) de l'api
     * @param parametersName - Nom des paramètres dans l'uri
     * @param parametersValue - Valeur des paramètres dans l'uri
     */
    public ApiRequestHandler(String[] paths, String[] parametersName, String[] parametersValue) {
        this(paths, parametersName, parametersValue, "");
    }

    /**
     * Constructeur
     * @param paths - Chemins (endpoints) de l'api
     * @param parametersName - Nom des paramètres dans l'uri
     * @param parametersValue - Valeur des paramètres dans l'uri
     * @param body - Corps de la requête
     */
    public <T> ApiRequestHandler(String[] paths, String[] parametersName, String[] parametersValue, T body) {
        this.apiPaths = paths;
        this.apiUriParametersName = parametersName;
        this.apiUriParametersValue = parametersValue;
        this.requestBody = body;
        this.gson = new Gson();
    }

    /**
     * Méthode qui permet d'envoyer la requête à l'api
     * @param req - Instance de l'énumération de toutes les requêtes
     */
    public void sendARequest(final Requests req) {
        Thread apiRequestThread = new Thread();
        switch (req) {
            case Authenticate:
                apiRequestThread = new Thread(this.proceedApiPostRequest(this.constructURI(), null));
                break;
            case CreateAnAccount:
                apiRequestThread = new Thread(this.proceedApiPostRequest(this.constructURI(), this.convertBodyToJson()));
                break;
            case GetUser:
            case GetUserHistory:
                apiRequestThread = new Thread(this.proceedApiGetRequest(this.constructURI()));
                break;
            case BindLockerToUser:
            case UnbindLockerOfUser:
            case UpdateUserHistory:
            case DeleteUserHistory:
                apiRequestThread = new Thread(this.proceedApiPutRequest(this.constructURI(), this.convertBodyToJson()));
                break;
            default:
                break;
        }
        this.runApiRequest(apiRequestThread);
    }

    /**
     * Méthode qui permet de savoir si la code de reponse du serveur est bon
     * @return - True si le code est entre 200 et 299
     */
    public boolean isApiResponseValid() {
        return this.apiResponseCode >= 200 && this.apiResponseCode <= 299;
    }

    /**
     * Méthode qui permet de récupérer la réponse du serveur désérialisé en objet <T>
     * @param responseType - Le .class qui représente le pattern de désérialisation pour la réponse serveur
     * @return - Instance de class pattern avec les données désérialisé du message
     */
    public <T> T getApiResponse(Class<T> responseType) {
        String response = this.apiResponse == null ? "" : this.apiResponse;
        Log.d("api_request", response);
        return this.gson.fromJson(response, responseType);
    }

    /**
     * Méthode qui permet de convertir le corps de la requête
     * @return - La requête converti en RequestBody
     */
    private RequestBody convertBodyToJson() {
        String jsonBody = this.gson.toJson(this.requestBody);
        return RequestBody.create(jsonBody, this.JSON);
    }

    /**
     * Méthode qui permet de construire l'uri pour la requête de l'api
     * @return - l'uri construire
     */
    private String constructURI() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .encodedAuthority(this.API_IP)
                .appendPath("api");

        for (String path: this.apiPaths) {
            builder.appendPath(path);
        }

        if (this.apiUriParametersValue.length == this.apiUriParametersName.length) {
            for (int i=0; i < this.apiUriParametersValue.length; i++) {
                builder.appendQueryParameter(this.apiUriParametersName[i], this.apiUriParametersValue[i]);
            }
        }

        return builder.build().toString();
    }

    /**
     * Méthode qui permet de créer un client avec l'acceptation de tous les certifacts ssl
     * @return - Instance d'un OkHttpClient
     */
    private OkHttpClient constructSslCertificateClient() {
        @SuppressLint("CustomX509TrustManager")
        X509TrustManager trustManager = new X509TrustManager() {
            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkClientTrusted(X509Certificate[] x509Certificates, String s) {
                // Aucune vérification client
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Override
            public void checkServerTrusted(X509Certificate[] x509Certificates, String s) {
                // Acceptation de tous les certificats sans effectuer de vérification
            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
        SSLContext sslContext;
        try {
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, new TrustManager[]{ trustManager }, null);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new RuntimeException(e);
        }

        return new OkHttpClient.Builder()
                .sslSocketFactory(sslContext.getSocketFactory(), trustManager)
                .build();
    }

    /**
     * Méthode qui permet de créer un Runnable pour une requête GET
     * @param apiUrl - uri de la requête
     * @return - Runnable
     */
    private Runnable proceedApiGetRequest(final String apiUrl) {
        return () -> {
            OkHttpClient client = this.constructSslCertificateClient();
            Request request = new Request.Builder()
                    .url(apiUrl)
                    .get()
                    .build();
            Log.d("api_request", apiUrl);
            Log.d("api_request", "Going to make the call to the api");
            Response response;
            try {
                response = client.newCall(request).execute();
                this.apiResponse = Objects.requireNonNull(response.body()).string();
                this.apiResponseCode = response.code();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    /**
     * Méthode qui permet de créer un Runnable pour une requête POST
     * @param apiUrl - uri de la requête
     * @param body - coprs de la requête
     * @return - Runnable
     */
    private Runnable proceedApiPostRequest(final String apiUrl, @Nullable RequestBody body) {
        return () -> {
            OkHttpClient client = this.constructSslCertificateClient();
            Request request = new Request.Builder()
                    .url(apiUrl)
                    .post(body == null ? RequestBody.create("", this.JSON) : body)
                    .build();
            Log.d("api_request", apiUrl);
            Log.d("api_request", "Going to make the call to the api");
            Response response;
            try {
                response = client.newCall(request).execute();
                this.apiResponse = Objects.requireNonNull(response.body()).string();
                this.apiResponseCode = response.code();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    /**
     * Méthode qui permet de créer un Runnable pour une requête PUT
     * @param apiUrl - uri de la requête
     * @param body - coprs de la requête
     * @return - Runnable
     */
    private Runnable proceedApiPutRequest(final String apiUrl, @Nullable RequestBody body) {
        return () -> {
            OkHttpClient client = this.constructSslCertificateClient();
            Request request = new Request.Builder()
                    .url(apiUrl)
                    .put(body == null ? RequestBody.create("", this.JSON) : body)
                    .build();
            Log.d("api_request", apiUrl);
            Log.d("api_request", "Going to make the call to the api");
            Response response;
            try {
                response = client.newCall(request).execute();
                this.apiResponse = Objects.requireNonNull(response.body()).string();
                this.apiResponseCode = response.code();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    /**
     * Méthode qui permet de créer un Runnable pour une requête DELETE
     * @param apiUrl - uri de la requête
     * @return - Runnable
     */
    private Runnable proceedApiDeleteRequest(final String apiUrl) {
        return () -> {
            OkHttpClient client = this.constructSslCertificateClient();
            Request request = new Request.Builder()
                    .url(apiUrl)
                    .delete()
                    .build();
            Log.d("api_request", apiUrl);
            Log.d("api_request", "Going to make the call to the api");
            Response response;
            try {
                response = client.newCall(request).execute();
                this.apiResponse = Objects.requireNonNull(response.body()).string();
                this.apiResponseCode = response.code();
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    /**
     * Méthode qui permet de lancer le Thread de la requête
     * @param t - Thread avec la requête sur l'api
     */
    private void runApiRequest(Thread t) {
        t.start();
        try {
            t.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
