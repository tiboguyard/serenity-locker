package com.example.serenitylocker.Middlewares;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.example.serenitylocker.R;

public class BluetoothHandler {
    public final int BLUETOOTH_ENABLE = 100;
    private final BluetoothAdapter bluetoothAdapter;
    private final Context context;

    public BluetoothHandler(Context context) {
        this.context = context;
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    /**
     * Cette méthode permet de demander à l'utilisateur d'autoriser le droit d'activer le bluetooth dans l'application
     */
    public void requestBluetoothAuthorization() {
        if (this.bluetoothAdapter == null) {
            Toast.makeText(this.context, this.context.getString(R.string.bluetooth_unavailable), Toast.LENGTH_LONG).show();
        } else {
            if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_DENIED) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                    ActivityCompat.requestPermissions((Activity) this.context, new String[]{Manifest.permission.BLUETOOTH_CONNECT}, this.BLUETOOTH_ENABLE);
                }
            }
        }
    }

    /**
     * Indique si le bluetooth est activé
     * @return - True si le bluetooth est activé
     */
    public boolean isBluetoothEnabled() {
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled();
    }

    /**
     * Cette méthode permet d'inverser l'état actuel du bluetooth si l'utilisateur à autoriser l'utilisation du bluetooth dans l'application
     */
    public void toggleBluetoothState() {
        if (this.bluetoothAdapter == null) {
            Toast.makeText(context, context.getString(R.string.bluetooth_unavailable), Toast.LENGTH_SHORT).show();
            return;
        }

        if (this.bluetoothAdapter.isEnabled()) {
            this.disableBluetooth();
        } else {
            this.enableBluetooth();
        }
    }

    /**
     * Cette méthode permet d'activer le bluetooth si l'utilisateur à autoriser l'utilisation du bluetooth dans l'application
     */
    public void enableBluetooth() {
        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED && !this.bluetoothAdapter.isEnabled()) {
            this.bluetoothAdapter.enable();

            // Ouverture des paramètres du bluetooth
            Intent intent = new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS);
            ((Activity) context).startActivityForResult(intent, this.BLUETOOTH_ENABLE);
        }
    }

    /**
     * Cette méthode permet de désactiver le bluetooth si l'utilisateur à autoriser l'utilisation du bluetooth dans l'application
     */
    public void disableBluetooth() {
        if (ActivityCompat.checkSelfPermission(this.context, Manifest.permission.BLUETOOTH_CONNECT) == PackageManager.PERMISSION_GRANTED && this.bluetoothAdapter.isEnabled()) {
            this.bluetoothAdapter.disable();
        }
    }

    /**
     * Méthode qui permet de gérer la réception des requêtes de permission pour le bluetooth
     * @param requestCode The request code
     * @param permissions The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *     which is either {@link android.content.pm.PackageManager#PERMISSION_GRANTED}
     *     or {@link android.content.pm.PackageManager#PERMISSION_DENIED}. Never null.
     *
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean allPermissionsGranted = true;
        if (requestCode == this.BLUETOOTH_ENABLE) {
            int i = 0;
            while (i < grantResults.length && allPermissionsGranted) {
                allPermissionsGranted = grantResults[i] == PackageManager.PERMISSION_GRANTED;
                i++;
            }
        }
        if (allPermissionsGranted) {
            this.enableBluetooth();
        }
    }
}
