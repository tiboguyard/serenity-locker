package com.example.serenitylocker.ViewModel;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.serenitylocker.Model.HistoryLine;
import com.example.serenitylocker.R;

import java.util.List;

public class HistoryFragmentAdapter extends RecyclerView.Adapter<HistoryFragmentViewHolder> {
    private List<HistoryLine> histories = null;

    public HistoryFragmentAdapter(List<HistoryLine> histories) {
        if (histories != null) {
            this.histories = histories;
        }
    }


    /**
     * Méthode qui permet de créer un vue pour chaque ligne d'historique
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return - Instance du viewHoolder pour une ligne d'historique
     */
    @NonNull
    @Override
    public HistoryFragmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View oneHistoryView = inflater.inflate(R.layout.one_history_line_viewer, parent, false);

        return new HistoryFragmentViewHolder(oneHistoryView);
    }

    /**
     * Méthode qui permet d'afficher une ligne d'historique en fonction de la position du scroll
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull HistoryFragmentViewHolder holder, int position) {
        HistoryLine hLine = histories.get(position);
        holder.render(hLine);
    }

    /**
     * Méthode qui permet de récupérer le nombre actuel de ligne d'historique
     * @return - le nombre de ligne d'historique
     */
    @Override
    public int getItemCount() {
        if(histories != null)
            return histories.size();
        return 0;

    }
}
