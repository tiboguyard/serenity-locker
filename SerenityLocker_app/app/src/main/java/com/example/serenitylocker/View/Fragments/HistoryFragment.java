package com.example.serenitylocker.View.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.serenitylocker.Middlewares.ApiRequestHandler;
import com.example.serenitylocker.Model.HistoryLine;
import com.example.serenitylocker.Model.User;
import com.example.serenitylocker.R;
import com.example.serenitylocker.ViewModel.HistoryFragmentAdapter;

import org.jetbrains.annotations.Contract;

import java.util.ArrayList;
import java.util.List;

public class HistoryFragment extends Fragment {
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private final List<HistoryLine> histories = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private String loggedUserId;

    public HistoryFragment() {}

    /**
     * Constructeur
     * @param userId - id bdd de la personne conecté
     */
    public HistoryFragment(String userId) {
        this.loggedUserId = userId;
    }
    @NonNull
    @Contract(" -> new")
    public static HistoryFragment newInstance() {
        return new HistoryFragment();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View historyFragmentView = inflater.inflate(R.layout.fragment_history, container, false);

        /**
         * Evénement qui permet de supprimer tout l'historique
         */
        ImageButton deleteHistory = historyFragmentView.findViewById(R.id.deleteHistory);
        deleteHistory.setOnClickListener(view -> {
            ApiRequestHandler apiHandler = new ApiRequestHandler(
                    new String[]{"Users", "Flushistory"},
                    new String[]{"id"},
                    new String[]{this.loggedUserId}
            );
            apiHandler.sendARequest(ApiRequestHandler.Requests.DeleteUserHistory);
            try {
                if (apiHandler.isApiResponseValid()) {
                    getApiHistoryDataForUser();
                } else {
                    Toast.makeText(getContext(), getString(R.string.remove_history_failed), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        /**
         * Evénement qui permet lors du swip en haut de la liste de refresh les lignes d'historique
         */
        swipeRefreshLayout = historyFragmentView.findViewById(R.id.historyRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::getApiHistoryDataForUser);

        // Création d'un layout manager qui acceuillera toutes les lignes d'historiques
        layoutManager = new LinearLayoutManager(getActivity());

        // Création de l'adapter sur la liste des lignes d'historique
        adapter = new HistoryFragmentAdapter(this.histories);

        // Configuration du recycler view
        recyclerView = (RecyclerView)historyFragmentView.findViewById(R.id.rvHistory);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(this.layoutManager);
        recyclerView.setAdapter(this.adapter);

        getApiHistoryDataForUser();

        return historyFragmentView;
    }

    /**
     * Méthode qui permet de récupérer l'historique du locker de la personne
     */
    public void getApiHistoryDataForUser() {
        ApiRequestHandler apiHandler = new ApiRequestHandler(new String[]{"Users", this.loggedUserId});
        apiHandler.sendARequest(ApiRequestHandler.Requests.GetUserHistory);
        try {
            if (apiHandler.isApiResponseValid()) {
                List<HistoryLine> newHistories;
                User apiResponse = apiHandler.getApiResponse(User.class);
                if (apiResponse.getLocker() != null && apiResponse.getLocker().getHistory() != null) {
                    newHistories = apiResponse.getLocker().getHistory();
                    histories.clear();
                    histories.addAll(newHistories);
                    this.refreshHistoryData();
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.empty_history), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Méthode qui permet de refresh les données du recyclerView
     */
    @SuppressLint("NotifyDataSetChanged")
    private void refreshHistoryData() {
        swipeRefreshLayout.setRefreshing(false);
        adapter.notifyDataSetChanged();
    }
}