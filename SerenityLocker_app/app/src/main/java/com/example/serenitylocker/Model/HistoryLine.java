package com.example.serenitylocker.Model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Cette classe représente les informations pour 1'enregistrement d'une modification d'état du locker
 */
public class HistoryLine {
    /**
     * Etat antérieur à la modification
     */
    @SerializedName("OldStatus")
    private Boolean oldStatus;
    /**
     * Nouvelle état suite à la modification
     */
    @SerializedName("NewStatus")
    private Boolean newStatus;
    /**
     * Date et heure de la modification d'état
     */
    @Nullable
    @SerializedName("TimeStamp")
    private String timeStamp;


    public HistoryLine(Boolean oldStatus, Boolean newStatus, @Nullable String timeStamp) {
        this.oldStatus = oldStatus;
        this.newStatus = newStatus;
        this.timeStamp = timeStamp;
    }


    public Boolean getOldStatus() {
        return oldStatus;
    }

    public void setOldStatus(Boolean oldStatus) {
        this.oldStatus = oldStatus;
    }

    public Boolean getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(Boolean newStatus) {
        this.newStatus = newStatus;
    }

    @Nullable
    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(@Nullable String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
