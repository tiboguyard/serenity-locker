package com.example.serenitylocker.View.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.serenitylocker.Middlewares.ApiRequestHandler;
import com.example.serenitylocker.Model.User;
import com.example.serenitylocker.R;
import com.example.serenitylocker.View.MainActivity;

import java.util.Objects;

public class LogInFragment extends Fragment {
    boolean isPasswordVisible = false;
    private SharedPreferences applicationData;
    public LogInFragment() {}
    @NonNull
    public static LogInFragment newInstance() {
        return new LogInFragment();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View logInFragmentView = inflater.inflate(R.layout.fragment_log_in, container, false);

        this.applicationData = requireContext().getSharedPreferences("LoggedUserInformations", Context.MODE_PRIVATE);

        EditText mailEditText = logInFragmentView.findViewById(R.id.mailAddress);
        EditText passwordEditText = logInFragmentView.findViewById(R.id.password);

        /**
         * Evénement qui permet de montrer ou cacher le mot de passe
         */
        Button showHidePasswordButton = logInFragmentView.findViewById(R.id.showHidePassword);
        showHidePasswordButton.setOnClickListener(view -> {
            isPasswordVisible = !isPasswordVisible;
            if (isPasswordVisible) {
                passwordEditText.setTransformationMethod(null);
                showHidePasswordButton.setText(R.string.hide_label);
            } else {
                passwordEditText.setTransformationMethod(new PasswordTransformationMethod());
                showHidePasswordButton.setText(R.string.show_label);
            }
        });

        /**
         * Evénement qui permet de gérer la pression sur le bouton Log In
         */
        Button logInButton = logInFragmentView.findViewById(R.id.logInButton);
        logInButton.setOnClickListener(view -> {
            if (!mailEditText.getText().toString().trim().equals("") && !passwordEditText.getText().toString().trim().equals("")) {
                ApiRequestHandler apiHandler = new ApiRequestHandler(
                        new String[]{"Users", "Authenticate"},
                        new String[]{"mail", "password"},
                        new String[]{mailEditText.getText().toString(), passwordEditText.getText().toString()}
                );
                apiHandler.sendARequest(ApiRequestHandler.Requests.Authenticate);
                try {
                    if (apiHandler.isApiResponseValid()) {
                        User apiResponse = apiHandler.getApiResponse(User.class);
                        // Sauvegarde de l'id de la personne dans le context de l'application
                        SharedPreferences.Editor editor = this.applicationData.edit();
                        editor.putString("loggedUserId", apiResponse.getId());
                        editor.apply();

                        // Lancement de l'activitée principal
                        Intent intent = new Intent(getContext(), MainActivity.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(getContext(), getString(R.string.wrong_credentials), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.wrong_form), Toast.LENGTH_SHORT).show();
            }
        });

        /**
         * Evénement qui permet de gérer la persistance pour les prochaines connexion
         */
        CheckBox persistAuthentication = logInFragmentView.findViewById(R.id.rememberAuthentication);
        persistAuthentication.setOnCheckedChangeListener((buttonView, isChecked) -> {
            SharedPreferences.Editor editor = applicationData.edit();
            editor.putBoolean("isLoginPersist", isChecked);
            editor.apply();
        });


        /**
         * Evénement qui permet de changer le "fragment" pour celui de la création de compte
         */
        Button signUpRequest = logInFragmentView.findViewById(R.id.signUp);
        signUpRequest.setOnClickListener(view -> {
            Fragment signUpFragment = new SignUpFragment();
            getParentFragmentManager().beginTransaction().replace(R.id.SignInUpFrame, signUpFragment).commit();
        });

        return logInFragmentView;
    }
}