package com.example.serenitylocker.Model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

/**
 * Cette classe représente les informations représentant un utilisateur de SerenityLocker
 */
public class User {
    @Nullable
    @SerializedName("Id")
    private String id;

    /**
     * Nom de la personne
     */
    @Nullable
    @SerializedName("Name")
    private String name;
    /**
     * Prénom de la personne
     */
    @Nullable
    @SerializedName("FirstName")
    private String firstName;
    /**
     * Mot de passe du compte de la personne
     */
    @Nullable
    @SerializedName("Password")
    private String password;
    /**
     * Mail de la personne
     */
    @Nullable
    @SerializedName("Mail")
    private String mail;
    /**
     * Informations du locker enregistré par la personne
     */
    @Nullable
    @SerializedName("Locker")
    private LockerInfos locker;


    @Nullable
    public String getId() {
        return id;
    }

    public void setId(@Nullable String id) {
        this.id = id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Nullable
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@Nullable String firstName) {
        this.firstName = firstName;
    }

    @Nullable
    public String getPassword() {
        return password;
    }

    public void setPassword(@Nullable String password) {
        this.password = password;
    }

    @Nullable
    public String getMail() {
        return mail;
    }

    public void setMail(@Nullable String mail) {
        this.mail = mail;
    }

    @Nullable
    public LockerInfos getLocker() {
        return locker;
    }

    public void setLocker(@Nullable LockerInfos locker) {
        this.locker = locker;
    }
}
