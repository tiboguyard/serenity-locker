package com.example.serenitylocker.View.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.serenitylocker.Middlewares.ApiRequestHandler;
import com.example.serenitylocker.R;

import org.jetbrains.annotations.Contract;

public class LockerFragment extends Fragment {
    private String loggedUserId;

    public LockerFragment() {}

    /**
     * Constructeur
     * @param userId - id bdd de la personne conecté
     */
    public LockerFragment(String userId) {
        this.loggedUserId = userId;
    }
    @NonNull
    @Contract(" -> new")
    public static LockerFragment newInstance() {
        return new LockerFragment();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View lockerFragmentView = inflater.inflate(R.layout.fragment_locker, container, false);

        /**
         * Evénement qui permet de gérer l'ajout d'un locker
         */
        EditText serialNumber = lockerFragmentView.findViewById(R.id.serialNumber);
        Button addSerial = lockerFragmentView.findViewById(R.id.addLockerSerial);
        addSerial.setOnClickListener(view -> {
            if(!serialNumber.getText().toString().trim().equals("")) {
                ApiRequestHandler apiHandler = new ApiRequestHandler(
                        new String[]{"Users", "addLocker"},
                        new String[]{"id", "serialNumber"},
                        new String[]{this.loggedUserId, serialNumber.getText().toString()}
                );
                apiHandler.sendARequest(ApiRequestHandler.Requests.BindLockerToUser);
                try {
                    if (apiHandler.isApiResponseValid()) {
                        serialNumber.setText("");
                        Toast.makeText(getContext(), getString(R.string.locker_added), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getContext(), getString(R.string.locker_not_added), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        /**
         * Evénement qui permet de gérer la suppresion d'un locker
         */
        Button removeLocker = lockerFragmentView.findViewById(R.id.removeALocker);
        removeLocker.setOnClickListener(view -> {
            ApiRequestHandler apiHandler = new ApiRequestHandler(
                    new String[]{"Users", "removeLocker"},
                    new String[]{"id"},
                    new String[]{this.loggedUserId}
            );
            apiHandler.sendARequest(ApiRequestHandler.Requests.UnbindLockerOfUser);
            try {
                if (apiHandler.isApiResponseValid()) {
                    Toast.makeText(getContext(), getString(R.string.locker_removed), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), getString(R.string.locker_not_removed), Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        return lockerFragmentView;
    }
}