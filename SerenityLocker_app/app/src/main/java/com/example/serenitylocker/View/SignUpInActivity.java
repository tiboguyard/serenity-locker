package com.example.serenitylocker.View;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.serenitylocker.Middlewares.BluetoothHandler;
import com.example.serenitylocker.R;
import com.example.serenitylocker.View.Fragments.LogInFragment;

public class SignUpInActivity extends AppCompatActivity {
    private SharedPreferences applicationData;
    private BluetoothHandler blHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_in);

        this.applicationData = getSharedPreferences("LoggedUserInformations", Context.MODE_PRIVATE);

        this.blHandler = new BluetoothHandler(this);
        this.blHandler.requestBluetoothAuthorization();

        /**
         * Vérification si l'utilisateur souhaite rester connécté et si il c'est déjà connecté avant
         */
        boolean isUserAlreadyLoggedAndPersistant = this.applicationData.contains("loggedUserId") &&
                this.applicationData.getString("loggedUserId", null) != null &&
                this.applicationData.contains("isLoginPersist") &&
                this.applicationData.getBoolean("isLoginPersist", false);
        if (isUserAlreadyLoggedAndPersistant) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            /**
             * Mise en place du "fragment" pour le formulaire de login
             */
            Fragment initialLoginForm = new LogInFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.SignInUpFrame, initialLoginForm).commit();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        this.blHandler.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onBackPressed() {
        // Désactivation du retour en arrière pour ne pas retourner à l'écran principal lors d'une déconexion
    }
}