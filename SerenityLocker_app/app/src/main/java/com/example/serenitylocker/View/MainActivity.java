package com.example.serenitylocker.View;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;

import com.example.serenitylocker.Middlewares.ApiRequestHandler;
import com.example.serenitylocker.Middlewares.BluetoothHandler;
import com.example.serenitylocker.Model.HistoryLine;
import com.example.serenitylocker.Model.User;
import com.example.serenitylocker.R;
import com.example.serenitylocker.View.Fragments.HistoryFragment;
import com.example.serenitylocker.View.Fragments.LockerFragment;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    private String loggedUserId;
    private Button closeOpenLocker;
    private ImageButton onOffBluetooth;
    private ApiRequestHandler apiHandler;
    private SharedPreferences applicationData;
    private Fragment historyFragment;
    private Fragment lockerFragment;
    private BluetoothHandler blHandler;


    /**
     * BroadcastReceiver pour les actions sur le bluetooth
     * Il permet de changer la couleur de l'indicateur d'activation du bluetooth sur l'interface
     */
    private final BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, @NonNull Intent intent) {
            String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                updateBluetoothButton(blHandler.isBluetoothEnabled());
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.applicationData = getSharedPreferences("LoggedUserInformations", Context.MODE_PRIVATE);
        this.loggedUserId = this.applicationData.getString("loggedUserId", null);

        /**
         * Ajout de l'écouteur de modification d'état d'activatrion du bluetooth
         */
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(bluetoothReceiver, filter);


        /**
         * Activation du bluetooth si autorisé par la personne
         */
        this.blHandler = new BluetoothHandler(this);
        this.blHandler.enableBluetooth();


        closeOpenLocker = findViewById(R.id.closeOpenLocker);

        this.historyFragment = new HistoryFragment(this.loggedUserId);
        this.lockerFragment = new LockerFragment(this.loggedUserId);

        /**
         * Récupération de l'état actuel du locker
         */
        apiHandler = new ApiRequestHandler(new String[]{"Users", this.loggedUserId});
        apiHandler.sendARequest(ApiRequestHandler.Requests.GetUser);
        try {
            if (apiHandler.isApiResponseValid()) {
                User apiResponse = apiHandler.getApiResponse(User.class);
                if (apiResponse.getLocker() != null) {
                    updateLockerButtonState(apiResponse.getLocker().getIsLocked());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /**
         * Evénement qui permet de modifier l'état du locker lors de l'appui sur le bouton
         */
        closeOpenLocker.setOnClickListener(view -> {
            if (isUserSteelHasALocker()) {
                String actualStatus = closeOpenLocker.getText().toString();
                Boolean isLockOpen = updateLockerButtonState(actualStatus.equals(getString(R.string.tap_to_close)));

                // Requête à l'api pour modifier le status de locker et l'ajouter dans l'historique
                Date currentDate = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss", Locale.getDefault());
                apiHandler = new ApiRequestHandler(
                        new String[]{"Users", "updatehistory"},
                        new String[]{"id"},
                        new String[]{this.loggedUserId},
                        new HistoryLine(isLockOpen, !isLockOpen, dateFormat.format(currentDate))
                );
                apiHandler.sendARequest(ApiRequestHandler.Requests.UpdateUserHistory);
                try {
                    if (apiHandler.isApiResponseValid()) {
                        ((HistoryFragment) historyFragment).getApiHistoryDataForUser();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // TODO : Faire l'appel au arduino depuis un ViewModel
            } else {
                Toast.makeText(this, getString(R.string.not_bound_locker), Toast.LENGTH_SHORT).show();
            }

        });

        /**
         * Evénement pour gérer l'ouverture du menu pop up
         */
        ImageButton imageButton = findViewById(R.id.configMenu);
        imageButton.setOnClickListener(this::showPopupMenu);

        /**
         * Evénement qui permet d'activer ou désactiver le bluetooth
         */
        onOffBluetooth = findViewById(R.id.activateDesactivateBluetooth);
        onOffBluetooth.setOnClickListener(view -> this.blHandler.toggleBluetoothState());

        /**
         * Mise en place du "fragment" pour la visualisation de l'historique
         */
        getSupportFragmentManager().beginTransaction().replace(R.id.mainContentDisplay, this.historyFragment).commit();
    }

    /**
     * Méthode qui permet de mettre à jour le bouton indiquant le status du locker
     * @param isLockedStatus - Status actuel d'ouverture ou de fermeture du locker
     * @return - Le nouveau status
     */
    private Boolean updateLockerButtonState(@NonNull Boolean isLockedStatus) {
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.rounded_button);

        int closeColor = ContextCompat.getColor(this, R.color.openStatus);
        int openColor = ContextCompat.getColor(this, R.color.highlight);

        Drawable mutableDrawable = Objects.requireNonNull(drawable).mutate();
        if (isLockedStatus) {
            closeOpenLocker.setText(R.string.tap_to_open);
            DrawableCompat.setTint(mutableDrawable, closeColor);
        } else {
            closeOpenLocker.setText(R.string.tap_to_close);
            DrawableCompat.setTint(mutableDrawable, openColor);
        }
        closeOpenLocker.setBackground(mutableDrawable);

        return !isLockedStatus;
    }

    /**
     * Méthode qui permet d'afficher un menu pop up qui affiche l'ensemble des menus de l'application
     * @param anchor - référence de la vue qui contiendra le menu
     */
    @SuppressLint("NonConstantResourceId")
    private void showPopupMenu(View anchor) {
        PopupMenu popupMenu = new PopupMenu(this, anchor);
        MenuInflater inflater = popupMenu.getMenuInflater();
        inflater.inflate(R.menu.menu, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.menu_history:
                    getSupportFragmentManager().beginTransaction().replace(R.id.mainContentDisplay, this.historyFragment).commit();
                    return true;
                case R.id.menu_locker:
                    getSupportFragmentManager().beginTransaction().replace(R.id.mainContentDisplay, this.lockerFragment).commit();
                    return true;
                case R.id.menu_logOut:
                    // Supression de l'id de l'utilisateur stocké
                    @SuppressLint("CommitPrefEdits")
                    SharedPreferences.Editor editor = this.applicationData.edit();
                    editor.remove("loggedUserId");
                    editor.apply();

                    // Retour au menu de login
                    Intent intent = new Intent(this, SignUpInActivity.class);
                    startActivity(intent);
                    return true;
                default:
                    return false;
            }
        });

        popupMenu.show();
    }

    /**
     * Méthode qui permet de vérifier si l'utilisateur à toujours un locker d'assigné
     * @return - true si l'utilisateur à toujours un locker
     */
    private boolean isUserSteelHasALocker() {
        apiHandler = new ApiRequestHandler(new String[]{"Users", this.loggedUserId});
        apiHandler.sendARequest(ApiRequestHandler.Requests.GetUser);
        try {
            if (apiHandler.isApiResponseValid()) {
                User apiResponse = apiHandler.getApiResponse(User.class);
                return apiResponse.getLocker() != null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Méthode qui permet de modifier l'état du bouton de bluetooth dans l'application
     * @param isBluetoothEnabled - True si le bluetooth est activé
     */
    private void updateBluetoothButton(boolean isBluetoothEnabled) {
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.bluetooth_icon);
        Drawable mutableDrawable = Objects.requireNonNull(drawable).mutate();

        int blOnColor = ContextCompat.getColor(this, R.color.openStatus);
        int blOffColor = ContextCompat.getColor(this, R.color.highlight);

        if (isBluetoothEnabled) {
            DrawableCompat.setTint(mutableDrawable, blOnColor);
        } else {
            DrawableCompat.setTint(mutableDrawable, blOffColor);
        }

        onOffBluetooth.setBackground(mutableDrawable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(bluetoothReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateBluetoothButton(this.blHandler.isBluetoothEnabled());
    }

    @Override
    public void onBackPressed() {
        // Désactivation du retour en arrière pour ne pas retourner à l'écran de login
    }
}