package com.example.serenitylocker.ViewModel;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.serenitylocker.Model.HistoryLine;
import com.example.serenitylocker.R;

public class HistoryFragmentViewHolder extends RecyclerView.ViewHolder {
    private final TextView oldStatus;
    private final TextView newStatus;
    private final TextView timeStamp;
    private HistoryLine historyInfos;

    public HistoryFragmentViewHolder(@NonNull final View itemView) {
        super(itemView);

        this.oldStatus = itemView.findViewById(R.id.oldStatus);
        this.newStatus = itemView.findViewById(R.id.newStatus);
        this.timeStamp = itemView.findViewById(R.id.timeStamp);
    }

    /**
     * Méthode qui permet de mettre à jour les données à afficher en fonction d'une ligne d'historique lors du scroll
     * @param historyInfos - Informations concerné une indication d'historique
     */
    public void render(final HistoryLine historyInfos) {
        this.historyInfos = historyInfos;

        int lockedStatus = ContextCompat.getColor(itemView.getContext(), R.color.openStatus);
        int unlockedStatus = ContextCompat.getColor(itemView.getContext(), R.color.highlight);

        String locked = itemView.getContext().getString(R.string.locked);
        String unlocked = itemView.getContext().getString(R.string.unlocked);

        // Modificatrion des valeurs par rapport au données d'une ligne d'historique passé en paramètre
        this.oldStatus.setText(this.historyInfos.getOldStatus() ? locked : unlocked);
        if (this.historyInfos.getOldStatus()) {
            this.oldStatus.setTextColor(lockedStatus);
        }else {
            this.oldStatus.setTextColor(unlockedStatus);
        }

        this.newStatus.setText(this.historyInfos.getNewStatus() ? locked : unlocked);
        if (this.historyInfos.getNewStatus()) {
            this.newStatus.setTextColor(lockedStatus);
        }else {
            this.newStatus.setTextColor(unlockedStatus);
        }

        this.timeStamp.setText(this.historyInfos.getTimeStamp());
    }
}
