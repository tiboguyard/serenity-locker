@echo off
echo Demande des droits administrateur...

:: Redirige le flux de sortie vers NUL
>nul 2>&1 (
    :: Exécute une commande en tant qu'administrateur
    net session >nul 2>&1
    if %errorLevel% equ 0 (
        echo Les droits administrateur sont déjà accordés.
    ) else (
        :: Relance le script en tant qu'administrateur
        echo Redémarrage en tant qu'administrateur...
        powershell Start-Process -FilePath "%0" -Verb RunAs
    )
)

dotnet dev-certs https --clean
dotnet dev-certs https --trust
dotnet dev-certs https --trust

pause