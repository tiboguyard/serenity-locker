﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using SerenityLockerAPI.Models;
using SerenityLockerAPI.Services;

namespace SerenityLockerAPI.Controllers
{
    [EnableCors("AllowAll")]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly UsersService _usersService;


        /// <summary>
        /// Constructeur des routes
        /// </summary>
        /// <param name="usersService">Référence vers le Service de requette mongodb pour les USERS</param>
        public UsersController(UsersService usersService) => _usersService = usersService;


        /// <summary>
        /// Route : api/Users -- Permet de récupérer tous les users
        /// </summary>
        [HttpGet]
        public async Task<List<User>> Get() => await _usersService.GetAsync();


        /// <summary>
        /// Route : api/Users/ID -- Permet de récupérer un user par son ID
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        [HttpGet("{id:length(24)}")]
        public async Task<ActionResult<User>> Get(string id) {
            User? user = await _usersService.GetAsync(id);
            return (user == null) ? NotFound() : Ok(user);
        }


        /// <summary>
        /// Route : api/Users/authenticate -- Permet de récupérer un user par son mail et mot de passe
        /// </summary>
        /// <param name="mail">Mail de l'utilisateur</param>
        /// <param name="password">Mot de passe de l'utilisateur</param>
        [HttpPost("Authenticate")]
        public async Task<ActionResult<User>> Post(string mail, string password) {
            User? user = await _usersService.GetAsync(mail, password);
            return (user == null) ? NotFound() : Ok(user);
        }
            

        /// <summary>
        /// Route : api/Users -- Permet de créer un nouvelle utilisateur
        /// </summary>
        /// <param name="newUser">Informations liées à l'utilisateur</param>
        [HttpPost]
        public async Task<IActionResult> Post(User newUser) {
            await _usersService.CreateAsync(newUser);
            return CreatedAtAction(nameof(Get), new { id = newUser.Id }, newUser);
        }


        /// <summary>
        /// Route : api/Users/ID -- Permet de mettre à jours les informations d'un utilisateur par son ID
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        /// <param name="updatedUser">Informations à mettre à jour pour l'utilisateur</param>
        [HttpPut("{id:length(24)}")]
        public async Task<IActionResult> Update(string id, User updatedUser) {
            User? user = await _usersService.GetAsync(id);

            if (user is null) { return NotFound(); }

            updatedUser.Id = user.Id;
            updatedUser.Name = updatedUser.Name is null ? user.Name : updatedUser.Name;
            updatedUser.FirstName = updatedUser.FirstName is null ? user.FirstName : updatedUser.FirstName;
            updatedUser.Password = updatedUser.Password is null ? user.Password : updatedUser.Password;
            updatedUser.Mail = updatedUser.Mail is null ? user.Mail : updatedUser.Mail;

            LockerInfos? lockerInfos = user.Locker;

            if (updatedUser.Locker is null) {
                updatedUser.Locker = lockerInfos;
            } else {
                LockerInfos? updatedLockerInfos = updatedUser.Locker;

                updatedLockerInfos.SerialNumber = updatedLockerInfos.SerialNumber is null ? lockerInfos.SerialNumber : updatedLockerInfos.SerialNumber;
                updatedLockerInfos.IsLocked = updatedLockerInfos.IsLocked is null ? lockerInfos.IsLocked : updatedLockerInfos.IsLocked;
                updatedLockerInfos.LastInteraction = updatedLockerInfos.LastInteraction is null ? lockerInfos.LastInteraction : updatedLockerInfos.LastInteraction;

                if (updatedLockerInfos.History is null) {
                    updatedLockerInfos.History = lockerInfos.History;
                }
                else {
                    List<HistoryLine?> mergedHistory = new List<HistoryLine?>(lockerInfos.History.Count + updatedLockerInfos.History.Count);
                    mergedHistory.AddRange(lockerInfos.History);
                    mergedHistory.AddRange(updatedLockerInfos.History);
                    updatedLockerInfos.History = mergedHistory;
                }
            }

            await _usersService.UpdateAsync(id, updatedUser);

            return NoContent();
        }


        /// <summary>
        /// Route : api/addLocker -- Permet de mettre changer le locker associé à une personne (remet toutes les informations du locker à zero !)
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        /// <param name="serialNumber">Numéro de série d'un locker</param>
        [HttpPut("addLocker")]
        public async Task<IActionResult> Update(string id, string serialNumber) {
            User addSerialNumber = new User();

            User? user = await _usersService.GetAsync(id);

            if (user is null) { return NotFound(); }

            addSerialNumber.Id = user.Id;
            addSerialNumber.Name = user.Name;
            addSerialNumber.FirstName = user.FirstName;
            addSerialNumber.Password = user.Password;
            addSerialNumber.Mail = user.Mail;
            addSerialNumber.Locker = new LockerInfos();

            LockerInfos? newSerialNumber = addSerialNumber.Locker;

            newSerialNumber.SerialNumber = serialNumber;
            newSerialNumber.IsLocked = null;
            newSerialNumber.LastInteraction = "";

            newSerialNumber.History = new List<HistoryLine>() { };

            await _usersService.UpdateAsync(id, addSerialNumber);

            return NoContent();
        }


        /// <summary>
        /// Route : api/updatehistory -- Permet de mettre à jour l'historique et la dernière action
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        [HttpPut("updatehistory")]
        public async Task<IActionResult> Update(string id, HistoryLine hLine)
        {
            User updateLockerHistory = new User();

            User? user = await _usersService.GetAsync(id);

            if (user is null) { return NotFound(); }

            updateLockerHistory.Id = user.Id;
            updateLockerHistory.Name = user.Name;
            updateLockerHistory.FirstName = user.FirstName;
            updateLockerHistory.Password = user.Password;
            updateLockerHistory.Mail = user.Mail;

            string lockerSerialNumber = user.Locker.SerialNumber;
            List<HistoryLine> history = user.Locker.History;
            updateLockerHistory.Locker = new LockerInfos();

            LockerInfos? newHistoryInfos = updateLockerHistory.Locker;

            newHistoryInfos.SerialNumber = lockerSerialNumber;
            newHistoryInfos.IsLocked = hLine.NewStatus;
            newHistoryInfos.LastInteraction = hLine.TimeStamp;

            if (history is null || history.Count == 0){
                history = new List<HistoryLine>();
            }
            history.Add(hLine);
            newHistoryInfos.History = history;

            await _usersService.UpdateAsync(id, updateLockerHistory);

            return NoContent();
        }


        /// <summary>
        /// Route : api/addLocker -- Permet de supprimer le locker associé à une personne (remet toutes les informations du locker à zero !)
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        /// <param name="serialNumber">Numéro de série d'un locker</param>
        [HttpPut("removeLocker")]
        public async Task<IActionResult> RemoveLocker(string id)
        {
            User removeLocker = new User();

            User? user = await _usersService.GetAsync(id);

            if (user is null) { return NotFound(); }

            removeLocker.Id = user.Id;
            removeLocker.Name = user.Name;
            removeLocker.FirstName = user.FirstName;
            removeLocker.Password = user.Password;
            removeLocker.Mail = user.Mail;
            removeLocker.Locker = null;

            await _usersService.UpdateAsync(id, removeLocker);

            return NoContent();
        }


        /// <summary>
        /// Route : api/Users/ID -- Permet de supprimer tout l'historique d'utilisation par rapport à un ID
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        [HttpPut("Flushistory")]
        public async Task<IActionResult> Update(string id) {
            User historyFlushed = new User();

            User? user = await _usersService.GetAsync(id);

            if (user is null) { return NotFound(); }

            historyFlushed.Id = user.Id;
            historyFlushed.Name = user.Name;
            historyFlushed.FirstName = user.FirstName;
            historyFlushed.Password = user.Password;
            historyFlushed.Mail = user.Mail;
            historyFlushed.Locker = new LockerInfos();

            LockerInfos? lockerInfos = user.Locker;
            LockerInfos? historyFlushedAttached = historyFlushed.Locker;

            historyFlushedAttached.SerialNumber = lockerInfos.SerialNumber;
            historyFlushedAttached.IsLocked = lockerInfos.IsLocked;
            historyFlushedAttached.LastInteraction = lockerInfos.LastInteraction;

            historyFlushedAttached.History = new List<HistoryLine>() { };

            await _usersService.UpdateAsync(id, historyFlushed);

            return NoContent();
        }


        /// <summary>
        /// Route : api/Users/ID -- Permet de supprimer un utilisateur par son ID
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        [HttpDelete("{id:length(24)}")]
        public async Task<IActionResult> Delete(string id) {
            User? user = await _usersService.GetAsync(id);

            if (user is null) { return NotFound(); }

            await _usersService.RemoveAsync(id);

            return NoContent();
        }
    }
}
