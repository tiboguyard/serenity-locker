using SerenityLockerAPI.Models;
using SerenityLockerAPI.Services;

/// ---------------------------------
/// CONFIGURATION DU BUILDER DE l'API
/// ---------------------------------


WebApplicationBuilder builder = WebApplication.CreateBuilder(args);


/// ----------------------------------------------
/// Ajout de la base de donn�e MongoDB au Services
/// ----------------------------------------------

// R�cup�ration des informations de la base de donn�es issue du fichier appsetings.json
builder.Services.Configure<SerenityLockerDatabaseSettings>(
    builder.Configuration.GetSection("SerenityLockerDatabase")
);

// Ajout du service pour la collection MongoDB Users
builder.Services.AddSingleton<UsersService>();

/// ----------------------------------------------

builder.Services.AddCors(options => {
    options.AddPolicy("AllowAll",
       builder => {
           builder.AllowAnyOrigin()
                      .AllowAnyMethod()
                      .AllowAnyHeader();
       });
});

// Ajout de l'ensemble des Controllers du projet
builder.Services.AddControllers().AddJsonOptions(
        options => options.JsonSerializerOptions.PropertyNamingPolicy = null);


builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


/// ---------------------------------
/// ---------------------------------


WebApplication app = builder.Build();

app.UseCors("AllowAll");

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();
