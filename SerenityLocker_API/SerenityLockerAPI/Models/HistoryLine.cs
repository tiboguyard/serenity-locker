﻿namespace SerenityLockerAPI.Models
{
    /// <summary>
    /// Cette classe représente les informations pour 1'enregistrement d'une modification d'état du locker
    /// </summary>
    public class HistoryLine
    {

        /// <summary>
        /// Etat antérieur à la modification
        /// </summary>
        public bool? OldStatus { get; set; }


        /// <summary>
        /// Nouvelle état suite à la modification
        /// </summary>
        public bool? NewStatus { get; set; }


        /// <summary>
        /// Date et heure de la modification d'état
        /// </summary>
        public string? TimeStamp { get; set; }
    }
}
