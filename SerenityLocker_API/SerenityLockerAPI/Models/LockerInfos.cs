﻿namespace SerenityLockerAPI.Models
{
    /// <summary>
    /// Cette classe représente les informations d'un Locker
    /// </summary>
    public class LockerInfos
    {

        /// <summary>
        /// Numéro de série unique à chaque Locker
        /// </summary>
        public string? SerialNumber { get; set; }


        /// <summary>
        /// Etat actuel du Locker
        /// true : le locker est en position fermé
        /// false : le locker est en position ouvert
        /// </summary>
        public bool? IsLocked { get; set; }


        /// <summary>
        /// Date et heure de la dernière modification d'état
        /// </summary>
        public string? LastInteraction { get; set; }


        /// <summary>
        /// Historique de toutes les modifications d'états
        /// </summary>
        public List<HistoryLine>? History { get; set; } 
    }
}
