﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SerenityLockerAPI.Models
{
    /// <summary>
    /// Cette classe représente les informations représentant un utilisateur de SerenityLocker
    /// </summary>
    public class User
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }


        /// <summary>
        /// Nom de la personne
        /// </summary>
        public string? Name { get; set; }


        /// <summary>
        /// Prénom de la personne
        /// </summary>
        public string? FirstName { get; set; }


        /// <summary>
        /// Mot de passe du compte de la personne
        /// </summary>
        public string? Password { get; set; }


        /// <summary>
        /// Mail de la personne
        /// </summary>
        public string? Mail { get; set; }


        /// <summary>
        /// Informations du locker enregistré par la personne
        /// </summary>
        public LockerInfos? Locker { get; set; }
    }
}
