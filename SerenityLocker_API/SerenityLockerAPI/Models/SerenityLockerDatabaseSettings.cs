﻿namespace SerenityLockerAPI.Models
{
    /// <summary>
    /// Cette classe représente les informations de connection à mongodb
    /// </summary>
    public class SerenityLockerDatabaseSettings
    {

        /// <summary>
        /// Liens de connexion vers la base de donnée
        /// </summary>
        public string? ConnectionString { get; set; }


        /// <summary>
        /// Nom de la base de donnée 
        /// </summary>
        public string? DatabaseName { get; set; }


        /// <summary>
        /// Nom de la Collection mongodb pour les utilisateurs
        /// </summary>
        public string? UsersCollectionName { get; set; }
    }
}
