﻿using MongoDB.Driver;
using Microsoft.Extensions.Options;
using SerenityLockerAPI.Models;

namespace SerenityLockerAPI.Services
{
    public class UsersService
    {
        // Collection de tous les users stockés en BDD
        private readonly IMongoCollection<User> _usersCollection;

        /// <summary>
        /// Constructeur pour le CRUD de requette sur la collection USERS
        /// </summary>
        /// <param name="serenityLockerDatabaseSettings">Informations de connexion à la BDD</param>
        public UsersService(IOptions<SerenityLockerDatabaseSettings> serenityLockerDatabaseSettings){
            MongoClient mongoClient = new MongoClient(serenityLockerDatabaseSettings.Value.ConnectionString);
            IMongoDatabase mongoDatabase = mongoClient.GetDatabase(serenityLockerDatabaseSettings.Value.DatabaseName);
            _usersCollection = mongoDatabase.GetCollection<User>(serenityLockerDatabaseSettings.Value.UsersCollectionName);
        }


        /// <summary>
        /// Permet de récupérer tous les utilisateurs
        /// </summary>
        /// <returns>L'ensemble des utilisateurs</returns>
        public async Task<List<User>> GetAsync() => await _usersCollection.Find(_ => true).ToListAsync();
         

        /// <summary>
        /// Permet de récupérer un utilisateur par son ID
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        /// <returns>Les informations liées à l'utilisateur</returns>
        public async Task<User?> GetAsync(string id) => await _usersCollection.Find(x => x.Id == id).FirstOrDefaultAsync();


        /// <summary>
        /// Permet de récupérer un utilisateur par son mail et mot de passe
        /// </summary>
        /// <param name="mail">Mail de l'utilisateur</param>
        /// <param name="password">Mot de passe de l'utilisateur</param>
        /// <returns>Les informations liées à l'utilisateur</returns>
        public async Task<User?> GetAsync(string mail, string password) => await _usersCollection.Find(x => x.Mail == mail && x.Password == password).FirstOrDefaultAsync();


        /// <summary>
        /// Permet de créer un utilisateur
        /// </summary>
        /// <param name="newUser">Informations concernants le nouvelle utilisateur</param>
        public async Task CreateAsync(User newUser) => await _usersCollection.InsertOneAsync(newUser);


        /// <summary>
        /// Permet de mettre à jours les informations d'un utilisateur en fonction de son ID
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        /// <param name="updatedUser">Les informations à mettre à jours</param>
        public async Task UpdateAsync(string id, User updatedUser) => await _usersCollection.ReplaceOneAsync(x => x.Id == id, updatedUser);


        /// <summary>
        /// Permet de supprimer un utilisateur de la BDD
        /// </summary>
        /// <param name="id">ID de l'utilisateur</param>
        public async Task RemoveAsync(string id) => await _usersCollection.DeleteOneAsync(x => x.Id == id);
    }
}
