NOM DE PROJET : Serenity Locker
SLOGAN INTERNE : Du moment qu'il y a une serrure il y a un client au bout ! 
SLOGAN : "Serenity Locker: La tranquillité d'esprit en toute confiance"
	
(Serrure connectée)
- System que l'on ajoute dans une porte qui permet de controller et de vérifier à distance que la porte soit fermée.
	- Site web (consulter l'historique de toutes les ouvertures et l'état actuel de la porte et le flux vidéo).
	- Appli (Consulter l'historique, le flux vidéo, l'état actuel de la porte, et pouvoir fermer ou ouvrir à distance).
	- Raccorder le system à une caméra (pour pouvoir voir qui est devant la porte avant de l'ouvrir à distance).
		- Caméra lié à une sonnete pour pouvoir lancer le flux vidéo avec une alerte sur le téléphone
	- Affichage à l'intérieur de la maison l'état de la porte (voir screen)
			
Optionnel : System de login (site web et appli)

<img src="./src/Images/lock-indoor-information.png">
