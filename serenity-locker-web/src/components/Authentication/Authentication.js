import React, { useRef, useState } from 'react';
import {useNavigate} from "react-router-dom";
import { authenticationRequest, registerRequest } from '../../controllers/apiRequestHandler';
import { INFINITE_COOCKIE } from '../../middlewares/coockie';

import './Authentication.css'

import appLogo from '../../assets/Images/site_icon.svg'
import USER_ICON from '../../assets/Images/USER_ICON/USER_ICON';
import LOCK_ICON from '../../assets/Images/LOCK_ICON/LOCK_ICON';


/**
 * This function allow the switch between the show and hide password input field
 * 
 * @param {Event} e - Event to catch the one that triggered the click (the span)
 */
function showHidePassword(e) {
    var passwordInput = document.getElementById("userPassword");
    var passWordInputValidation = document.getElementById("passwordValidation");

    if(passWordInputValidation !== null) {
        if (passWordInputValidation.type === "password") {
            passWordInputValidation.type = "text";
        } else {
            passWordInputValidation.type = "password";
        }
    }

    if (passwordInput.type === "password") {
        passwordInput.type = "text";
        e.target.innerHTML="Hide";
        return;
    } 

    passwordInput.type = "password";
    e.target.innerHTML="Show";
}


/**
 * This arrow function handle the creation of the register form
 * 
 * @param {useRef} firstNameRef - Reference to the first name value entered by the user
 * @param {useRef} lastNameRef - Reference to the last name value entered by the user
 * @param {useRef} emailRef - Reference to the mail value entered by the user
 * @param {useRef} passwordRef - Reference to the password value entered by the user
 * @param {useNavigate} navigate - Navigator to switch between views
 * @param {useState} switchForm - Reference to the useState to handle the switch between form register and login
 * @returns JSX Node object
 */
const registerForm = (firstNameRef, lastNameRef, emailRef, passwordRef, navigate, switchForm) => {

    /**
     * This function handle the submit request of the form
     * 
     * @param {Event} event - click event
     * @returns 
     */
    function handleSubmit(event) {
        event.preventDefault();

        const firstName = firstNameRef.current.value;
        const lastName = lastNameRef.current.value;
        const email = emailRef.current.value;
        const password = passwordRef.current.value;

        let passwordValidation = document.getElementById('passwordValidation').value;

        if(password !== passwordValidation) {
            alert('Passwords are not the same !');
            return;
        }

        registerRequest({
            Name: lastName,
            FirstName: firstName,
            Password: password,
            Mail: email,
            Locker: null
        }, navigate);
    }


    return (
        <>
            <form id="authenticationForm"
                  className='formHolder'
                  name="registerForm" 
                  onSubmit={handleSubmit}>
                
                <h1 className='formTitle'>Sign Up</h1> 

                <div className='formMainContentHolder'>
                    <div className='formInputHolder formTwoColumns'>
                        <input required
                               id="userFirstName"
                               className='formInputHolderBorder formInputField' 
                               type="text" 
                               name="firstName" 
                               ref={firstNameRef}
                               placeholder="First Name"/>

                        <input required
                               id="userLastName"
                               className='formInputHolderBorder formInputField' 
                               type="text" 
                               name="lastName" 
                               ref={lastNameRef}
                               placeholder="Last Name"/>
                    </div>

                    <div className='formInputHolder formInputHolderBorder'>
                        {USER_ICON()}

                        <input required
                               id="userMail"
                               className='formInputField' 
                               type="email" 
                               name="mail" 
                               ref={emailRef}
                               placeholder="Mail address"/>
                    </div>

                    <div className='formInputHolder formInputHolderBorder'>
                        {LOCK_ICON()}

                        <input required 
                               id="userPassword" 
                               className='formInputField' 
                               type="password" 
                               name="password"
                               ref={passwordRef}
                               placeholder="Password"/>

                        <span className="hideShowButton" onClick={(e) => showHidePassword(e)}>Show</span>
                    </div>

                    <div className='formInputHolder formInputHolderBorder'>
                        {LOCK_ICON()}

                        <input required 
                               id="passwordValidation" 
                               className='formInputField' 
                               type="password" 
                               name="password"
                               placeholder="Confirm Password"/>
                    </div>

                </div>

                <button className='formSubmitButton formInputHolderBorder' type="submit">Sign up</button>

            </form>
            <div className='formRowTextHolder'>
                <span>Already have an account ?</span>
                <span className='formClickableText' onClick={() => switchForm('loginForm')}>Sign in</span>
            </div>
        </>
    )
}

/**
 * This arrow function handle the creation of the login form
 * 
 * @param {useRef} emailRef - Reference to the mail value entered by the user
 * @param {useRef} passwordRef - Reference to the password value entered by the user
 * @param {useNavigate} navigate - Navigator to switch between views
 * @param {useState} switchForm - Reference to the useState to handle the switch between form register and login
 * @returns JSW node object
 */
const loginForm = (emailRef, passwordRef, navigate, switchForm) => {

    /**
     * This function handle the submit request of the form
     * 
     * @param {Event} event - Click event
     */
    function handleSubmit(event) {
        event.preventDefault();
        const email = emailRef.current.value;
        const password = passwordRef.current.value;
        const checkbox = document.getElementById("rememberCheckbox");
        if (checkbox.checked) { 
            authenticationRequest(email, password, navigate, INFINITE_COOCKIE);
        } else {
            authenticationRequest(email, password, navigate, 1); 
        }
    }


    return (
        <>
            <form id="authenticationForm"
                  className='formHolder'
                  name="loginForm" 
                  onSubmit={handleSubmit}>
                
                <h1 className='formTitle'>Log In</h1> 

                <div className='formMainContentHolder'>
                    <div className='formInputHolder formInputHolderBorder'>
                        {USER_ICON()}

                        <input required
                               id="userMail"
                               className='formInputField' 
                               type="email" 
                               name="mail" 
                               ref={emailRef}
                               placeholder="Mail address"/>
                    </div>

                    <div className='formInputHolder formInputHolderBorder'>
                        {LOCK_ICON()}

                        <input required 
                               id="userPassword" 
                               className='formInputField' 
                               type="password" 
                               name="password"
                               ref={passwordRef}
                               placeholder="Password"/>

                        <span className="hideShowButton" onClick={(e) => showHidePassword(e)}>Show</span>
                    </div>

                    <div id="formOptionnal">
                        <div>
                            <input type="checkbox"
                                   id="rememberCheckbox" 
                                   name="rememberMe"/>
                            <span>Remember me</span>
                        </div>
                        <span className='formClickableText' onClick={() => {}}>Forgot Password</span>
                    </div>
                </div>

                <button className='formSubmitButton formInputHolderBorder' type="submit">Log In</button>

            </form>
            <div className='formRowTextHolder'>
                <span>Don't have an account ?</span>
                <span className='formClickableText' onClick={() => switchForm('registerForm')}>Sign up</span>
            </div>
        </>
    )
}


/**
 * This function is a React component that create a custom login or register form
 * 
 * @returns JSX node object
 */
function Authentication() {
    const navigate = useNavigate();

    const [form, setForm] = useState('loginForm');

    const firstNameRef = useRef(null);
    const lastNameRef = useRef(null);
    const emailRef = useRef(null);
    const passwordRef = useRef(null);

    return (
        <div id="authentication">

            <div id='appName'> 
                <img className='website_logo' 
                     src={appLogo} 
                     alt='icon du site'/>
                <span>Serenity Locker</span>
            </div>

            {form === 'loginForm' && loginForm(emailRef, passwordRef, navigate, setForm)}
            {form === 'registerForm' && registerForm(firstNameRef, lastNameRef, emailRef, passwordRef, navigate, setForm)}

            <span id='cpr'>© Copyright all rights reserved</span>
        </div>
    )
}

export default Authentication;