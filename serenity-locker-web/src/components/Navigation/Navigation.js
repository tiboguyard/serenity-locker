import React from 'react';
import {useNavigate} from "react-router-dom";

import './Navigation.css'

import appLogo from '../../assets/Images/site_icon.svg'


/**
 * This function is a React component that create the navigation menu
 * 
 * @param {Props} props - Arguments passed at the creation of the component
 * @returns JSX node object
 */
function Navigation(props) {
    const navigate = useNavigate();


    /**
     * This funtion is called at the initialization of the component to define the menu to select
     * 
     * @param {Props} props - properties passed at the creation of the component
     * @param {String} actualMenu - name of the menu in the nav
     * @returns ClassName
     */
    function setSelected(props, actualMenu) {
        const historyState = props.History !== undefined ? props.History.selected : undefined;
        const LockerState = props.Locker !== undefined ? props.Locker.selected : undefined;

        if (historyState !== undefined && actualMenu === "HISTORY") {
            if (historyState === true) {
                return "selected";
            }
        } else if (LockerState !== undefined && actualMenu === "LOCKER") {
            if (LockerState === true) {
                return "selected";
            }
        }
    }


    return (
        <div id="navigation">
            <div id='appName'> 
                <img className='website_logo' 
                     src={appLogo} 
                     alt='icon du site'/>
                <span>Serenity Locker</span>
            </div>
            <nav>
                <ul>
                    <li onClick={() => navigate("/history")} className={setSelected(props, "HISTORY")}>History</li>
                    <li onClick={() => navigate("/locker")} className={setSelected(props, "LOCKER")}>Locker</li>
                </ul>
            </nav>
        </div>
    )
}


export default Navigation;