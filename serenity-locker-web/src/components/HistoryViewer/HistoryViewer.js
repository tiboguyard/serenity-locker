
import { deleteHistoryRequest } from '../../controllers/apiRequestHandler';
import { reverseArray } from '../../middlewares/arrayProcessing';

import TRASH_ICON from '../../assets/Images/TRASH_ICON/TRASH_ICON';

import './HistoryViewer.css'


/**
 * This function is a React component that create the History view
 * 
 * @param {Props} props - Arguments passed at the creation of the component
 * @returns JSX node object
 */
function HistoryViewer(props) {
    var historyData = props.HistoryData;
    const userId = props.userId;


    /**
     * This function handle the delete history request
     */
    function handleDeleteHistory() {
        deleteHistoryRequest(userId).then(serverResponse => {
            if(serverResponse) {
                window.location.reload();
            } else {
                alert("Error while trying to erase the history !");
            }
        });
    }

    
    return (
        <div id="historyViewer">
            <div id='historyHeader'>
                <h1>Histories</h1>
                <button id="trashButton" onClick={() => handleDeleteHistory()}>
                    <span>Delete history</span>
                    {TRASH_ICON()}
                </button>
            </div>
            <div id="historyLines">
                <span id="fixScrollViewerReverse"/>
                {historyData !== null && historyData !== "" && reverseArray(historyData).map(row => (
                    <div key={row.TimeStamp + Date.now().toPrecision().toString()} className="historyLine">
                        <span className='historyEntry'>
                            <span className='historyLabel'>Old state :</span>
                            <span className={
                                    row.OldStatus.toString() === "true" ? "Locked" : "UnLocked"
                                }>{row.OldStatus.toString() === "true" ? "Locked" : "Unlocked"}</span>
                        </span>
                        <span className='historyEntry'>
                            <span className='historyLabel'>New state :</span>
                            <span className={
                                    row.NewStatus.toString() === "true" ? "Locked" : "UnLocked"
                                }>{row.NewStatus.toString() === "true" ? "Locked" : "Unlocked"}</span>
                        </span>
                        <span className='historyEntry'>
                            <span className='historyLabel'>Time stamp :</span>
                            <span className='textInfo'>{row.TimeStamp}</span>
                        </span>
                    </div>
                ))}
            </div>
        </div>
    )

}

export default HistoryViewer;