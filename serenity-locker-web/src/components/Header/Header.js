import React from 'react';
import { useNavigate } from "react-router-dom";
import DropdownMenu from '../DropdownMenu/DropdownMenu';

import { USER_ICON_ALTERNATIVE } from '../../assets/Images/USER_ICON/USER_ICON';
import LOGOUT_ICON from '../../assets/Images/LOGOUT_ICON/LOGOUT_ICON';

import './Header.css'


/**
 * This function is a React component that create the header
 * 
 * @param {Props} props - Arguments passed at the creation of the component
 * @returns JSX node object
 */
function Header(props) {
    const navigate = useNavigate();

    const userDatas = props.UserDatas;
    const lockerDatas = props.LockerDatas;

    /**
     * This function handle the opening of the dropdown menu of the profile
     */
    function handleOpenProfile() {
        const profileButton = document.getElementById("profileButton");

        const profileElements = {
            profile: {
                type: "button",
                text: "Profile",
                navigate: "/profile"
            },
            logout: {
                type: "button",
                style: "text-image",
                text: "Log out",
                image : LOGOUT_ICON(),
                navigate: "/",
                resetCoockie: "loginCoockie"
            }
        }

        var cstrDropdownMenu = new DropdownMenu(profileButton, profileElements, navigate);
        document.querySelector("header").appendChild(cstrDropdownMenu.getGeneratedNode());
        cstrDropdownMenu.startMouseLeaveListener();
    }

    return (
        <header>
            <button id='profileButton' onClick={() => handleOpenProfile()}>
                <span id='userName'>{userDatas.FName}</span>
                {USER_ICON_ALTERNATIVE()}
            </button>
            <span className='rowTextHolder'>
                <span>User id : </span>
                <span id='userId' className='textInfo'>{userDatas.Id}</span>
            </span>
            <span className='rowTextHolder'>
                <span>Locker serial number : </span>
                <span id='lockerSerialNumber' className='textInfo'>{lockerDatas.SerialNumber}</span>
            </span>
            <div id='lastLockerStateInfos'>
                <span className='rowTextHolder'>
                    <span>Actual locker state : </span>
                    <span id='actualLockerState' className={lockerDatas.LockerState}/>
                </span>
                <span className='rowTextHolder'>
                    <span>Timestamp : </span>
                    <span id='actualTimestamp' className='textInfo'>{lockerDatas.TimeStamp}</span>
                </span>
            </div>
        </header>
    )
}

export default Header;