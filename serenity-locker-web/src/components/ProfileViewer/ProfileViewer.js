import React, { useRef } from 'react';
import { deleteSessionCoockie } from '../../middlewares/coockie';
import { modifyUserInformationRequest, deleteUserRequest } from '../../controllers/apiRequestHandler';

import './ProfileViewer.css';

import TRASH_ICON from '../../assets/Images/TRASH_ICON/TRASH_ICON';
import USER_ICON from '../../assets/Images/USER_ICON/USER_ICON';
import LOCK_ICON from '../../assets/Images/LOCK_ICON/LOCK_ICON';


const HIDE_PASSWORD = "************";

/**
 * This function allow the switch between the show and hide password input field
 * 
 * @param {Event} e - Event to catch the one that triggered the click (the span)
 */
function showHidePassword(e) {
    var passwordInput = document.getElementById("userPassword");
    var passWordInputValidation = document.getElementById("passwordValidation");

    if(passWordInputValidation !== null) {
        if (passWordInputValidation.type === "password") {
            passWordInputValidation.type = "text";
        } else {
            passWordInputValidation.type = "password";
        }
    }

    if (passwordInput.type === "password") {
        passwordInput.type = "text";
        e.target.innerHTML="Hide";
        return;
    } 

    passwordInput.type = "password";
    e.target.innerHTML="Show";
}


/**
 * This function is a React component that create the profile view
 * 
 * @param {Props} props - Arguments passed at the creation of the component
 * @returns JSX node object
 */
function ProfileViewer(props) {
    const userDatas = props.UserDatas
    const userId = userDatas.Id;
    const ufirstName = userDatas.FName;
    const ulastName = userDatas.LName;
    const uMail = userDatas.Mail;
    const uPassword = userDatas.Password;

    const firstNameRef = useRef(null);
    const lastNameRef = useRef(null);
    const emailRef = useRef(null);
    const passwordRef = useRef(null);


    /**
     * This function handle the hide and show of the user password of the header
     * 
     * @param {Event} e - click event
     * @returns 
     */
    function showHideUserPassword(e) {
        e.preventDefault();
        var passwordInput = document.getElementById("userPasswordHeader");
        if (passwordInput.innerHTML === HIDE_PASSWORD && e.target.innerHTML === "Show") {
            passwordInput.innerHTML = uPassword;
            e.target.innerHTML="Hide";
            return;
        }   

        passwordInput.innerHTML = HIDE_PASSWORD;
        e.target.innerHTML="Show";
    }


    /**
     * Thus function handle tge delete of the user account
     */
    function handleDeleteAccount() {
        deleteUserRequest(userId).then(serverResponse => {
            if(serverResponse) {
                deleteSessionCoockie("loginCoockie");
                window.location.reload();
            } else {
                alert("Error while trying to delete the account !");
            }
        });
    }


    /**
     * This function handle the modifications of the user information (first and last name)
     * 
     * @param {Event} event - click event
     */
    function handleSubmitModifyInformation(event) {
        event.preventDefault();

        const firstName = firstNameRef.current.value;
        const lastName = lastNameRef.current.value;


        if (firstName.trim() === "" && lastName.trim() !== "") {
            modifyUserInformationRequest(userId, {Name: lastName.trim()}).then(serverResponse => {
                if(serverResponse) {
                    window.location.reload();
                } else {
                    alert("Error while trying to change the last name !");
                }
            });
        } else if (lastName.trim() === "" && firstName.trim() !== "") {
            modifyUserInformationRequest(userId, {FirstName: firstName.trim()}).then(serverResponse => {
                if(serverResponse) {
                    window.location.reload();
                } else {
                    alert("Error while trying to change the first name !");
                }
            });
        } else if (firstName.trim() !== "" && lastName.trim() !== "") {
            modifyUserInformationRequest(userId, {FirstName: firstName.trim(), Name: lastName.trim()}).then(serverResponse => {
                if(serverResponse) {
                    window.location.reload();
                } else {
                    alert("Error while trying to change the first and last name !");
                }
            });
        }
        
    }


    /**
     * This function handle the modification of the user mail
     * 
     * @param {Event} event - click event
     * @returns 
     */
    function handleSubmitChangeMailAddress(event) {
        event.preventDefault();

        const email = emailRef.current.value;
        let emailValidation = document.getElementById("userMailValidation").value;

        if(email !== emailValidation) {
            alert('Mail addresses are not the same !');
            return;
        }

        modifyUserInformationRequest(userId, {Mail: email}).then(serverResponse => {
            if(serverResponse) {
                window.location.reload();
            } else {
                alert("Error while trying to change the mail address !");
            }
        });    
    }


    /**
     * This function handle the modification of the user password
     * 
     * @param {Event} event - click event
     * @returns 
     */
    function handleSubmitChangePassword(event) {
        event.preventDefault();

        const password = passwordRef.current.value;
        let passwordValidation = document.getElementById("passwordValidation").value;

        if(password !== passwordValidation) {
            alert('Passwords are not the same !');
            return;
        }

        modifyUserInformationRequest(userId, {Password: password}).then(serverResponse => {
            if(serverResponse) {
                window.location.reload();
            } else {
                alert("Error while trying to change the password !");
            }
        }); 
    }



    return (
        <div id="profileViewer">
            <div id='profileHeader'>
                <h1>Profile</h1>
                <button id="trashButton" onClick={() => handleDeleteAccount()}>
                    <span>Delete account</span>
                    {TRASH_ICON()}
                </button>
                <div id='lastLockerStateInfos'>
                    <span className='rowTextHolder'>
                        <span>First name : </span>
                        <span id='userFnameHeader' className='textInfo'>{ufirstName}</span>
                    </span>
                    <span className='rowTextHolder'>
                        <span>Last name : </span>
                        <span id='userLnameHeader' className='textInfo'>{ulastName}</span>
                    </span>
                </div>
                <span className='rowTextHolder'>
                    <span>Mail address : </span>
                    <span id='userMailHeader' className='textInfo'>{uMail}</span>
                </span>
                <span className='rowTextHolder'>
                    <span>Password : </span>
                    <span id='userPasswordHeader' className='textInfo'>{HIDE_PASSWORD}</span>
                    <span className="hideShowButton" onClick={(e) => showHideUserPassword(e)}>Show</span>
                </span>
            </div>
            <div id="profileAction">
                <div id="twoFormColumn">
                    <form id="formModifyInformation"
                        className='formHolder'
                        name="modifyUserInformation" 
                        onSubmit={handleSubmitModifyInformation}>
                        
                        <h1 className='formTitle'>Modify user information</h1> 

                        <div className='formMainContentHolder'>
                            <div className='formInputHolder formTwoColumns'>
                                <input id="userFirstName"
                                    className='formInputHolderBorder formInputField' 
                                    type="text" 
                                    name="firstName" 
                                    ref={firstNameRef}
                                    placeholder="First Name"/>

                                <input id="userLastName"
                                    className='formInputHolderBorder formInputField' 
                                    type="text" 
                                    name="lastName"
                                    ref={lastNameRef}
                                    placeholder="Last Name"/>
                            </div>

                        </div>

                        <button className='formSubmitButton formAlignHolderRight formInputHolderBorder' type="submit">Modify</button>

                    </form>

                    <form id="formModifyUserMail"
                          className='formHolder'
                          name="modifyUserInformation" 
                          onSubmit={handleSubmitChangeMailAddress}>
                        
                        <h1 className='formTitle'>Change mail address</h1> 

                        <div className='formMainContentHolder'>
                            <div className='formInputHolder formInputHolderBorder'>
                                {USER_ICON()}

                                <input required
                                    id="userMail"
                                    className='formInputField' 
                                    type="email" 
                                    name="mail"
                                    ref={emailRef}
                                    placeholder="Mail address"/>

                            </div>

                            <div className='formInputHolder formInputHolderBorder'>
                                {USER_ICON()}

                                <input required
                                    id="userMailValidation"
                                    className='formInputField' 
                                    type="email" 
                                    name="mail"
                                    placeholder="Confirm Mail address"/>
                            </div>
                        </div>

                        <button className='formSubmitButton formAlignHolderRight formInputHolderBorder' type="submit">Change</button>

                    </form>
                </div>   

                <form id="formModifyPassword"
                      className='formHolder'
                      name="modifyUserPassword" 
                      onSubmit={handleSubmitChangePassword}>
                    
                    <h1 className='formTitle'>Change password</h1> 
                    <div className='formMainContentHolder'>
                        <div className='formInputHolder formInputHolderBorder'>
                            {LOCK_ICON()}

                            <input required 
                                id="userPassword" 
                                className='formInputField' 
                                type="password" 
                                name="password"
                                ref={passwordRef}
                                placeholder="Password"/>

                            <span className="hideShowButton" onClick={(e) => showHidePassword(e)}>Show</span>
                        </div>
                        <div className='formInputHolder formInputHolderBorder'>
                            {LOCK_ICON()}
                            
                            <input required 
                                id="passwordValidation" 
                                className='formInputField' 
                                type="password" 
                                name="password"
                                placeholder="Confirm Password"/>
                        </div>
                    </div>
                    <button className='formSubmitButton formAlignHolderRight formInputHolderBorder' type="submit">Change</button>
                </form>
            </div>
        </div>
    )
}

export default ProfileViewer;