import React, { useRef } from 'react';
import { addLockerRequest, removeLockerRequest } from '../../controllers/apiRequestHandler';

import './LockerViewer.css'


/**
 * This function is a React component that create the Locker view
 * 
 * @param {Props} props - Arguments passed at the creation of the component
 * @returns JSX node object
 */
function LockerViewer(props) {
    const userId = props.Id;

    const newSerialNumberRed = useRef(null);


    /**
     * This function handle the addition of a new locker
     * 
     * @param {Event} event - click event
     */
    function handleAddSubmit(event) {
        event.preventDefault();
        const serialNumber = newSerialNumberRed.current.value;
        addLockerRequest(serialNumber, userId).then(serverResponse => {
            if(serverResponse) {
                window.location.reload();
            } else {
                alert("Error while trying to add the serial number !");
            }
        });

    }


    /**
     * This function handle the suppression of a user locker
     */
    function handleDeleteSubmit() {
        removeLockerRequest(userId).then(serverResponse => {
            if(serverResponse) {
                window.location.reload();
            } else {
                alert("Error while trying to add the serial number !");
            }
        });
    }


    return (
        <div id="lockerForms">

            <form id="newLockerForm"
                  name="newLockerForm" 
                  onSubmit={handleAddSubmit}>
                        
                <h1 className='formTitle'>Add locker</h1> 

                <div className='formMainContentHolder'>
                    <div className='formInputHolderBorder borderFlex'>
                        <input required
                               id="serialNumber"
                               className='formInputField' 
                               type="text" 
                               name="serialNumber"
                               ref={newSerialNumberRed}
                               placeholder="Serial number"/>
                    </div>
                </div>

                <button className='formSubmitButton formAlignHolderRight formInputHolderBorder' type="submit">Add</button>

            </form>

            <div id="deleteLockerForm">
                        
                <h1 className='formTitle'>Delete locker</h1> 

                <button className='formSubmitButton formInputHolderBorder' onClick={() => handleDeleteSubmit()}>Remove</button>

            </div>
        </div>
    )
}

export default LockerViewer;