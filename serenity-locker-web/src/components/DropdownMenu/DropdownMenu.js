import ReactDomServer from 'react-dom/server';
import { deleteSessionCoockie } from '../../middlewares/coockie';

import './DropdownMenu.css';


/**
 * This class is used to create a dropdown menu based on a given element
 */
export default class DropdownMenu {

    /**
     * Constructor 
     * 
     * @param {Node} referencedElement - Parent element on witch the dropdown menu will be created
     * @param {Object} navigationElements - Object that contains all the element that will be present in the dropdown menu
     * @param {useNavigate} navigator - React useNavigate() to navigate to an other page
     */
    constructor(referencedElement, navigationElements, navigator) {
        this.mainNode = document.createElement('div');
        this.navNode = document.createElement("div");
        this.referencedElementSizeProperties = {
            width: referencedElement.getClientRects()[0].width,
            height: referencedElement.getClientRects()[0].height
        };
        this.referencedElementPositionProperties = {
            x: referencedElement.getClientRects()[0].x,
            y: referencedElement.getClientRects()[0].y
        };
        this.navigationElements = navigationElements;

        this.navigator = navigator;

        this.mainNodeId = "dropdownMenu";
        this.mainNode.id = this.mainNodeId;
        this.navNodeId = "dropdownMenuNavigationHolder";
        this.navNode.id = this.navNodeId;
    }

    /**
     * This function generate the dropdown menu
     * 
     * @returns Node
     */
    getGeneratedNode() { 
        this._setNavigationElements();

        this._defineMainNodeStyle();

        this.mainNode.appendChild(this.navNode);
        return this.mainNode; 
    }


    /**
     * This function start an event listener to close the dropdown menu when the mouse leave it
     */
    startMouseLeaveListener() {
        const createdProfileNode = document.getElementById(this.mainNodeId);
        createdProfileNode.addEventListener("mouseleave", function() {
            document.querySelector("header").removeChild(createdProfileNode);
        });
    }


    /**
     * This function generate all the dropdown menu items
     */
    _setNavigationElements() {
        Object.entries(this.navigationElements).forEach(([key, value]) => {

            var generatedNavigationElementNode;

            if (typeof value === "object") {
                Object.entries(value).forEach(([key, value]) => {
                    switch (key) {
                        case "type" :
                            generatedNavigationElementNode = document.createElement(value.toLowerCase());
                            break;
                        case "style" :
                            const newStyleHolder = document.createElement("div");
                            newStyleHolder.style.display = "flex";
                            newStyleHolder.style.flexDirection = "row";
                            newStyleHolder.style.justifyContent = "center";
                            newStyleHolder.style.alignItems = "center";
                            newStyleHolder.style.gap = "15px";

                            const textHolder = document.createElement("div");
                            const imgHolder = document.createElement("div");
                            
                            switch (value) {
                                case "text-image" :
                                    generatedNavigationElementNode.className = "text-image";
                                    textHolder.className = "text";
                                    imgHolder.className = "image";

                                    newStyleHolder.appendChild(textHolder);
                                    newStyleHolder.appendChild(imgHolder);
                                    break;
                                case "image-text" :
                                    generatedNavigationElementNode.className = "image-text";
                                    textHolder.className = "image-text text";
                                    imgHolder.className = "image-text image";

                                    newStyleHolder.appendChild(imgHolder);
                                    newStyleHolder.appendChild(textHolder);
                                    break;
                                default :
                                    break;
                            }

                            generatedNavigationElementNode.appendChild(newStyleHolder);
                            break;
                        case "text" : 
                                this._handleStylePartArrangement(generatedNavigationElementNode, "text", value);
                            break;
                        case "image" :
                                if(typeof value === "object") {
                                    this._handleStylePartArrangement(generatedNavigationElementNode, "image", this._convertJSXToString(value));
                                } else {
                                    this._handleStylePartArrangement(generatedNavigationElementNode, "image", value);
                                }
                            break;
                        case "navigate" :
                            generatedNavigationElementNode.addEventListener("click", () => {
                                this.navigator(value)
                            });
                            break;
                        case "resetCoockie" :
                            generatedNavigationElementNode.addEventListener("click", () => {
                                deleteSessionCoockie(value);
                            });
                            break;
                        default :
                            break;
                    }
                });
                this.navNode.appendChild(generatedNavigationElementNode);
            }
        });
    }


    /**
     * This function handle the specific style arrangement on an item
     * 
     * @param {Node} parent - Node with the specific style
     * @param {String} part - "text" or "image", the searched class name on the children's of "parent"
     * @param {String} value - The value that will be given in the innerHTML if the part is found
     */
    _handleStylePartArrangement(parent, part, value) {
        if (parent.innerHTML === "") {
            parent.innerHTML = value;
        } else {
            Array.prototype.forEach.call(parent.children[0].children, stylePart => {
                Array.prototype.forEach.call(stylePart.classList, className => {
                    if (className === part) {
                        stylePart.innerHTML = value;
                    }
                });
                
            });
        }
    }


    /**
     * This function convert a JSX node object in string
     * 
     * @param {JSX} value - JSW node object
     * @returns String
     */
    _convertJSXToString(value) {
        return ReactDomServer.renderToString(value);
    }


    /**
     * This function handle the position and the size of the dropdown menu each time he's rendered
     */
    _defineMainNodeStyle() {
        this.mainNode.style.gridTemplateRows = this.referencedElementSizeProperties.height+"px auto";

        this.mainNode.style.left = this.referencedElementPositionProperties.x+"px";
        this.mainNode.style.top = this.referencedElementPositionProperties.y+"px";

        this.mainNode.style.width = this.referencedElementSizeProperties.width+"px";
    }
}