
/**
 * This function reverse the order of the elements in an array
 * 
 * @param {Array} array - The array to reverse
 * @returns Array
 */
const reverseArray = (array) => {
    const arrayReferencedLength = array.length -1;
    return array.map((item, itemId) => array[arrayReferencedLength - itemId]);
}


export {
    reverseArray
}