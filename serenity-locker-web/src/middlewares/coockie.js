
const INFINITE_COOCKIE = -1;

/**
 * This function create a cookie for a limited amount of time
 * 
 * @param {String} coockieName - name of the coockie that will be created
 * @param {*} data - data that will be stored in the coockie
 * @param {*} aliveTime - number of days that will be alive before the coockie is destroyed
 */
const createSessionCoockie = (coockieName, data, aliveTime) => {
    if (aliveTime === INFINITE_COOCKIE) {
        document.cookie = coockieName + '=' + data + "; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
    } else {
        var expirationDate = new Date(Date.now() + (aliveTime * 24 * 60 * 60 * 1000)).toUTCString();
        document.cookie = coockieName + '=' + data + "; expires="+ expirationDate + "; path=/";
    }
}


/**
 * This function is used to retrieve a coockie data
 * 
 * @param {String} coockieName - name of the coockie to find
 * @returns 
 */
const getSessionCoockie = (coockieName) => {
    coockieName += "=";
    var coockies = document.cookie.split(";");
    for (var i = 0; i < coockies.length; i++) {
        var coockie = coockies[i];
        while (coockie.charAt(0) === " ") {
            coockie = coockie.substring(1, coockie.length);
        }
        if (coockie.indexOf(coockieName) === 0) {
            return coockie.substring(coockieName.length, coockie.length);
        }
    }
    return null;
}


/**
 * This function is used to remove a coockie
 * 
 * @param {String} coockieName - name of the coockie to remove
 */
const deleteSessionCoockie = (coockieName) => {
    coockieName += "=";
    document.cookie = coockieName+" ; expires = Thu, 01 Jan 1970 00:00:00 GMT"
}


export {
    INFINITE_COOCKIE,
    createSessionCoockie,
    getSessionCoockie,
    deleteSessionCoockie
}