import React, { useEffect, useState } from 'react';
import {useNavigate} from "react-router-dom";
import { getSessionCoockie } from '../middlewares/coockie';
import { getUserDatasRequest } from '../controllers/apiRequestHandler';

import Header from '../components/Header/Header';
import Navigation from '../components/Navigation/Navigation';
import LockerViewer from '../components/LockerViewer/LockerViewer';

import '../assets/Styles/Default.css';
import '../assets/Styles/Form.css';


/**
 * This function is responsible of the Locker page
 * 
 * @returns JSX node object
 */
function Locker() {
    const navigate = useNavigate();

    const [userId, setUserId] = useState(null);
    const [userName, setUserName] = useState(null);
    const [lockerSerialNumber, setLockerSerialNumber] = useState(null);
    const [actualLockerState, setActualLockerState] = useState(null);
    const [actualTimestamp, setActualTimestamp] = useState(null);

    useEffect(() => {
        const coockieId = getSessionCoockie("loginCoockie");

        if (coockieId != null) {
            getUserDatasRequest(coockieId).then(userDatas => {
                setUserId(userDatas.Id);
                setUserName(userDatas.FirstName);
                if (userDatas.Locker === null || userDatas.Locker === undefined || userDatas.Locker.SerialNumber === undefined) {
                    setLockerSerialNumber("none");
                    setActualLockerState("");
                    setActualTimestamp("No registered actions");
                } else {
                    setLockerSerialNumber(userDatas.Locker.SerialNumber);
                    setActualLockerState(userDatas.Locker.IsLocked ? "Locked" : userDatas.Locker.IsLocked === null ? "" : "UnLocked");
                    setActualTimestamp(userDatas.Locker.LastInteraction === "" ? "No registered actions" : userDatas.Locker.LastInteraction);
                }
              }); 
        } else {
            navigate("/"); // Go back to the login page
        }
    }, [userId]); 


    return (
        <div id='baseFrame'>

            <Navigation 
                Locker={{ 
                    selected: true 
                }}/>

            <div id='mainFrame'>

                <Header 
                    UserDatas={{
                        Id: userId,
                        FName: userName
                    }}
                    LockerDatas={{
                        SerialNumber: lockerSerialNumber,
                        LockerState: actualLockerState,
                        TimeStamp: actualTimestamp
                    }}/>

                    <LockerViewer Id={userId}/>
            </div>

        </div>
    )
}

export default Locker;