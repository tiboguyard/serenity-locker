import React, { useEffect, useState } from 'react';
import {useNavigate} from "react-router-dom";
import { getSessionCoockie } from '../middlewares/coockie';
import { getUserDatasRequest } from '../controllers/apiRequestHandler';

import Header from '../components/Header/Header';
import Navigation from '../components/Navigation/Navigation';
import HistoryViewer from '../components/HistoryViewer/HistoryViewer';

import '../assets/Styles/Default.css';
import '../assets/Styles/Form.css';


/**
 * This function is responsible of the History page
 * 
 * @returns JSX node object
 */
function History() {
    const navigate = useNavigate();

    const [userId, setUserId] = useState(null);
    const [userName, setUserName] = useState(null);
    const [lockerSerialNumber, setLockerSerialNumber] = useState(null);
    const [actualLockerState, setActualLockerState] = useState(null);
    const [actualTimestamp, setActualTimestamp] = useState(null);
    const [actualLockerHistory, setActualLockerHistory] = useState(null);

    useEffect(() => {
        const coockieId = getSessionCoockie("loginCoockie");

        if (coockieId != null) {
            getUserDatasRequest(coockieId).then(userDatas => {
                setUserId(userDatas.Id);
                setUserName(userDatas.FirstName);
                if (userDatas.Locker === null || userDatas.Locker === undefined || userDatas.Locker.SerialNumber === undefined) {
                    setLockerSerialNumber("none");
                    setActualLockerState("");
                    setActualTimestamp("No registered actions");
                    setActualLockerHistory("");
                } else {
                    setLockerSerialNumber(userDatas.Locker.SerialNumber);
                    setActualLockerState(userDatas.Locker.IsLocked ? "Locked" : userDatas.Locker.IsLocked === null ? "" : "UnLocked");
                    setActualTimestamp(userDatas.Locker.LastInteraction === "" ? "No registered actions" : userDatas.Locker.LastInteraction);
                    setActualLockerHistory(userDatas.Locker.History);
                }
              }); 
        } else {
            navigate("/"); // Go back to the login page
        }
    }, [userId]); 

    return (
        <div id='baseFrame'>

            <Navigation 
                History={{ 
                    selected: true 
                }}/>

            <div id='mainFrame'>

                <Header 
                    UserDatas={{
                        Id: userId,
                        FName: userName
                    }}
                    LockerDatas={{
                        SerialNumber: lockerSerialNumber,
                        LockerState: actualLockerState,
                        TimeStamp: actualTimestamp
                    }}/>

                <HistoryViewer userId={userId} 
                               HistoryData={actualLockerHistory}/>

            </div>

        </div>
    )
}

export default History;