import React, { useEffect } from 'react';
import {useNavigate} from "react-router-dom";
import { getSessionCoockie } from '../middlewares/coockie';

import Authentication from '../components/Authentication/Authentication';

import '../assets/Styles/Default.css';
import '../assets/Styles/Form.css';


/**
 * This function is responsible of the Authenticate page
 * 
 * @returns JSX node object
 */
function Authenticate() {
    const navigate = useNavigate();

    useEffect(() => {
        if (getSessionCoockie("loginCoockie") != null) {
            navigate("/history");
        }
    }); 

    return (
        <>
            <Authentication />
        </>
    )
}

export default Authenticate;