
import { createSessionCoockie } from "../middlewares/coockie";

const apiURL = 'https://192.168.1.78:45455/api/';

/**
 * This function handle the authentication request to the API
 * 
 * @param {String} userMail - Mail entered by the user 
 * @param {String} userPass - Password entered by the user
 * @param {useNavigate} navigator - If the user exist, where to redirect the user
 * @param {Integer} alive - Number of days the authentication coockie will be alive
 */
const authenticationRequest = (userMail, userPass, navigator, alive) => {
    fetch(apiURL+'Users/Authenticate?' + new URLSearchParams({
        mail: userMail,
        password: userPass
    }), {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(data => {
        if(data.Id !== undefined) {
            if (navigator !== "" && navigator !== null) {
                createSessionCoockie("loginCoockie", data.Id, alive);
                navigator("/history");
            }
        } else {
            alert("Aucun utilisateur existe avec ses informations !");
        }
    })
    .catch(error => {
        console.log(error);
    });
}


/**
 * This function handle the register request to the API
 * 
 * @param {Object} dataObj - Object that contains all the information about a new user
 * @param {useNavigate} navigator - After the user is created, where to redirect the user 
 */
const registerRequest = (dataObj, navigator) => {
    fetch(apiURL+'Users', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataObj)
    })
    .then(response => response.json())
    .then(data => {
        if(data.Id !== undefined) {
            if (navigator !== "" && navigator !== null) {
                createSessionCoockie("loginCoockie", data.Id, 1);
                navigator("/history");
            }
        } else {
            alert("Erreur lors de la création de l'utilisateur !");
        }
    })
    .catch(error => {
        console.log(error);
    });
}


/**
 * This function allow to retrieve all the data of a user based on he's ID
 * 
 * @param {String} userId - Id of the user in the BDD
 * @returns Object containing all the data of the user
 */
const getUserDatasRequest = async (userId) => {
    const response = await fetch(apiURL+'Users/' + userId, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(data => {
        return data;
    })
    .catch(error => {
        console.log(error);
    });
    return response;
}


/**
 * This function allow to erase the history of a locker based on the linked user ID
 * 
 * @param {String} userId - Id of the user in the BDD
 * @returns true if the erase as succeeded
 */
const deleteHistoryRequest = async (userId) => {
    var serverResponse = true;
    await fetch(apiURL+'Users/Flushistory?' + new URLSearchParams({
        id: userId
    }), {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .catch(error => {
        serverResponse = false;
    });
    return serverResponse;
}


/**
 * This function handle the addition of a new locker based on a user ID
 * 
 * @param {String} SerialNumber - Serial number of the locker
 * @param {String} userId - Id of the user in the BDD
 * @returns rue if the addition as succeeded
 */
const addLockerRequest = async (SerialNumber, userId) => {
    var serverResponse = true;
    await fetch(apiURL+'Users/addLocker?' + new URLSearchParams({
        id: userId,
        serialNumber: SerialNumber
    }), {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .catch(error => {
        serverResponse = false;
    });
    return serverResponse;
}


/**
 * This function allow to remove a locker based on the linked user ID
 * 
 * @param {String} userId - Id of the user in the BDD
 * @returns true if the remove as succeeded
 */
const removeLockerRequest = async (userId) => {
    var serverResponse = true;
    await fetch(apiURL+'Users/removeLocker?' + new URLSearchParams({
        id: userId
    }), {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .catch(error => {
        serverResponse = false;
    });
    return serverResponse;
}


/**
 * This function handle the modifications of the user information based on his ID
 * 
 * @param {String} userId - Id of the user in the BDD
 * @param {Object} dataObj - Information to modify
 * @returns true if the modifications as succeeded
 */
const modifyUserInformationRequest = async (userId, dataObj) => {
    var serverResponse = true;
    await fetch(apiURL+'Users/' + userId, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(dataObj)
    })
    .catch(error => {
        serverResponse = false;
    });
    return serverResponse;
}


/**
 * This function handle the delete of a user account base on his ID
 * 
 * @param {String} userId - Id of the user in the BDD
 * @returns true if the delete as succeeded
 */
const deleteUserRequest = async (userId) => {
    var serverResponse = true;
    await fetch(apiURL+'Users/' + userId, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .catch(error => {
        serverResponse = false;
    });
    return serverResponse;
}


export {
    authenticationRequest,
    registerRequest,
    getUserDatasRequest,
    deleteHistoryRequest,
    addLockerRequest,
    removeLockerRequest,
    modifyUserInformationRequest,
    deleteUserRequest
}
    