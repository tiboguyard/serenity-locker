import ProjectRoutes from "./routes.js";

function App() {
  return (
    <div>
      <ProjectRoutes />
    </div>
  );
}

export default App;