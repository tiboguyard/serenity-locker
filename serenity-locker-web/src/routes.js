import React from "react";
import {BrowserRouter, Routes, Route} from "react-router-dom";

import Authenticate from "./webpages/Authenticate";
import History from "./webpages/History";
import Locker from "./webpages/Locker";
import Profile from "./webpages/Profile";


/**
 * This function provide all the routes of the project
 * 
 * @returns JSX with all the routes of the project
 */
function ProjectRoutes() {
    return (
        <BrowserRouter>
            <Routes>
                <Route index element={<Authenticate />} />
                <Route path="/history" element={<History />} />
                <Route path="/locker" element={<Locker />} />
                <Route path="/profile" element={<Profile />} />
            </Routes>
        </BrowserRouter>
    )
};


export default ProjectRoutes;